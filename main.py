#Напишите функцию которая принемает *args
tachki = [
    {
        'maker': 'honda fit',
        'cc': 1300
    },
    {
        'maker': 'toyota alteza',
        'cc': 2500
    },
    {
        'maker': 'subaru forester',
        'cc': 2500
    }
]
tachki_2 = [
    {
        'maker': 'mazda demio',
        'cc': 1300
    },
    {
        'maker': 'mitsubishi outlander',
        'cc': 2500
    },
    {
        'maker': 'suzuki alto',
        'cc': 1000
    }
]
def filter(*args):
    print(*args, sep='\n')
filtered_tachki = filter(tachki, tachki_2, ['gonka taksistov'])

# Напишите функцию которая принемает *kwargs

def voenkomat(**kwargs):
    print('--fio_i_dannye--', kwargs)
voenkomat(first_name='Timur', last_name='Bolotov', age=21,gorod='Bishkek', street='Almatinka',domN=55)

# # Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным

taxi = [
    {
        'maker': 'honda fit',
        'cc': 1300
    },
    {
        'maker': 'toyota alteza',
        'cc': 2500
    },
    {
        'maker': 'subaru forester',
        'cc': 2500
    }
]
taxi_2 = [
    {
        'maker': 'mazda demio',
        'cc': 1300
    },
    {
        'maker': 'mitsubishi outlander',
        'cc': 2400
    },
    {
        'maker': 'suzuki alto',
        'cc': 1000
    }
]
def filter(*args):
    normalnaya_tachka = []
    global motor
    for avto in args:
        for motor in avto:
            if motor['cc'] >= 2400:
                normalnaya_tachka.append(motor)

    return normalnaya_tachka

normalnaya_tachka = filter(taxi, taxi_2)
print('Рекомендуется ставить ГБО, без этого деньги на ветер--', normalnaya_tachka)
#
# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор),
# но с использованием параметров args, kwargs


girls = [
    {
        'name': 'Aijana',
        'age': 20
    },
    {
        'name': 'Jibek',
        'age': 18
    },
    {
        'name': 'Ajara',
        'age': 25
    }
]
mens = [
    {
        'name': 'Nurbek',
        'age': 20
    },
    {
        'name': 'Adilet',
        'age': 18
    },
    {
        'name': 'Nursultan',
        'age': 25
    }
]
mens_2 = [
    {
        'name': 'Maloi',
        'age': 19
    },
    {
        'name': 'Baha mestnyi',
        'age': 27
    }
]


def filter(*args, **kwargs):
    for tolpa in args:
        for molodej in tolpa:
            if molodej['age'] >= 20:
                print(f"{kwargs['secure']}:{molodej['name']} даем браслет")
                print(f"{kwargs['admin']}: с печатью {molodej['name']}\n")
            else:
                print(f"{kwargs['secure']}: {molodej['name']} без обид ребята\n")


filter(girls, mens, mens_2, admin='администратор', secure='айрон')
